(module notes
  (load-notes load-note note->sxml note-title note-date
   note-text note-summary merge-notes group-by-date)
  (import scheme chicken utils data-structures extras srfi-1 srfi-13
          srfi-19-date srfi-19-io posix files lowdown fmt sxpath)

  (use fmt srfi-19-io srfi-19-date lowdown sxpath)

  (define-record note date count text)

  (define (same-date? note-1 note-2)
    (date=? (note-date note-1) (note-date note-2)))

  (define (note-summary note)
    (let ((title (note-title note))
          (count (note-count note))
          (date (format-date "~Y/~m/~d" (note-date note))))
      (fmt #f (dsp date) (space-to 14)
           "[" (dsp count) "]" (space-to 22)
           (fmt-join dsp title "; "))))

  (define (note->sxml note)
    (receive (doc _)
      (markdown->sxml* (note-text note))
      `(note ,@doc)))

  (define (valid-heading heading)
    (and (equal? 1 (second heading))
         (not (equal? "TODO" (third heading)))
         (not (string-contains (third heading) "vim:"))))

  (define (note-title note)
    (let ((sxml (note->sxml note))
          (selector (sxpath '(// heading))))
      (fold 
        (lambda (heading agg)
          (if (valid-heading heading)
            (cons (third heading) agg)
            agg))
        '() 
        (selector sxml))))

  (define (merge-notes note-1 note-2)
    (note-count-set! note-1 (+ (note-count note-1) (note-count note-2)))
    (note-text-set! note-1 (conc (note-text note-1) "\n" (note-text note-2))))

  (define (load-note-from path)
    (call-with-input-file path (lambda (port) (load-note path port))))

  (define (load-note path port)
    (let ((directory (pathname-directory path)))
      (receive (_ _ elements)
               (decompose-directory directory)
        (let* ((date-parts (map string->number (take-right elements 3)))
               (year (first date-parts))
               (month (second date-parts))
               (day (third date-parts))
               (date (make-date 0 0 0 0 day month year)))
          (make-note date 1 (read-all port))))))

  (define (load-notes basedir #!key (loader standard-load)
                                    (pattern "^.*.mkd"))
    (find-files basedir test: pattern action: loader))

  (define (standard-load path prev)
    (cons (load-note-from path) prev))

  (define (group-by-date path prev)
    (let ((note (load-note-from path)))
      (cond
        ((= 0 (length prev))
         (cons note prev))
        ((same-date? note (car prev))
          (merge-notes (car prev) note)
          prev)
        (else
          (cons note prev)))))

  (define ((date-pattern date) path)
    #t)

)
