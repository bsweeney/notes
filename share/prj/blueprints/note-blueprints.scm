(use (prefix srfi-19 date:) uuid)
(import (prefix variables var:)
        (prefix substitution sub:))

(define (template raw)
  (sub:substitute raw (var:variables)))

(blueprint note-base ()
  (variables
    (uuid ,uuid-v4)
    (date ,(date:format-date "~1" (date:current-date))))
  (directories (,template "$uuid"))
  (files
    ((,template "$uuid/note.mkd") note/note-base.mkd)))

(blueprint note (note-base)
  (files
    ((,template "$uuid/note.mkd") note/note.mkd)))

(blueprint short-note (note-base)
  (files
    ((,template "$uuid/note.mkd") note/short-note.mkd)))
