#!/usr/bin/env bash

# Usage: nte index [options] [file]
# Summary: Index note files.
# Help: Index all or a given note. Currently this is a no-op
#
# Options:
#   -a        Index all notes, empties the DB first.
#   -c        Clear all notes

set -euo pipefail
IFS=$'\n\t'

index() 
{
  file="$1"
  [ -e "$file" ] || error "File $file to index does not exist"
  mdix index --database "$_NTE_DB" "$file"
}

files() 
{
  find "$_NTE_NOTES" -name 'note.mkd'
}

clear()
{
  mdix clear --database "$_NTE_DB"
}

while getopts "a" opt; do
  case $opt in
    "a" )
      clear
      for file in $(files); do
        index "$file"
      done
      exit 0
      ;;
    "c" )
      clear
      ;;
    * )
      error "Unknown option. See help"
      ;;
  esac
done

shift $((OPTIND - 1))

index "$1"
