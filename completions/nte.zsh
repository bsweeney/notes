if [[ ! -o interactive ]]; then
    return
fi

compctl -K _nte nte

_nte() {
  local word words completions
  read -cA words
  word="${words[2]}"

  if [ "${#words}" -eq 2 ]; then
    completions="$(nte commands)"
  else
    completions="$(nte completions "${word}")"
  fi

  reply=("${(ps:\n:)completions}")
}
