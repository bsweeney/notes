# Notes #

This is a set of highly idiosyncratic scripts for me to manage notes related to
coding. This is sort of like an electronic lab notebook crossed with a todo list
organizer.

My goal with this is to provide a way to take notes about a project which are
linked to the code as well as to other things such as emails, papers, websites
etc. For details on my thought process and motivation see the notes I took when
developing this.

This is mostly written as a series of simple shell scripts, some more advanced
parts will require a real language, but the basic functions are in shell.

# Requirements #

* ag
* lua
* xapian
  - lua bindings

<!-- To come
* notmuch
-->
